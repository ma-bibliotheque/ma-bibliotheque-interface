import { computed, createApp, reactive } from 'vue'
import { createRouter, createWebHistory } from 'vue-router'

import VueCookies from 'vue3-cookies'
import { useCookies } from "vue3-cookies"

import App from './App.vue'

import Login from './components/Login.vue'
import BookList from './components/BookList.vue'
import AddBook from './components/AddBook.vue'

import Oruga from '@oruga-ui/oruga-next'
import { bulmaConfig } from '@oruga-ui/theme-bulma'

import '@oruga-ui/theme-bulma/dist/bulma.css'

/* import the fontawesome core */
import { library } from '@fortawesome/fontawesome-svg-core'

/* import font awesome icon component */
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

/* import specific icons */
import { fas } from '@fortawesome/free-solid-svg-icons'

/* add icons to the library */
library.add(fas);

const apiURL = 'http://localhost:8000'

const connexionURL = apiURL + '/login'


const user = reactive({
  connected: false,
  id: null,
  username: null
})

function login(credentials) {

  const { cookies } = useCookies()
  const csrfToken = cookies.get('csrftoken')

  let data = new FormData()
  data.append('username', credentials.username)
  data.append('password', credentials.password)

  fetch(
    connexionURL,
    {
      method: "POST",
      body: data,
      credentials: 'include',
      headers: { "X-CSRFToken": csrfToken },
      }
    )
    .then((response) => response.json())
    .then((status) => {
        if (status.user !== null) {
          user.connected = true
          user.id = status.user.id
          user.username = status.user.username

          router.push({name: 'home'})
        }
      })
}

function logout() {

  fetch(
    apiURL + '/logout',
    {
      method: 'GET',
      credentials: 'include'
    }
  )
  .then((response) => response.json())
  .then(() => {
    user.connected = false
    user.id = null
    user.username = null
    router.push({name: 'login'})
  })
}

const routes = [
  {
    path: '/',
    name: 'home',
    component: BookList,
    meta: {requiresAuth: true}
    },
  {
    path: '/login',
    name: 'login',
    component: Login,
    meta: {requiresAuth: false}
    },
  {
    path: '/addBook',
    name: 'addBook',
    component: AddBook,
    meta: {requiresAuth: true}
    },
  ]

await fetch(apiURL + '/connexion_status', {credentials: 'include'})
  .then((response) => response.json())
  .then((status) => {
    if (status.user !== null) {
      user.connected = true
      user.id = status.user.id
     user.username = status.user.username
    }
  })

const router = createRouter({
  history: createWebHistory(),
  routes: routes
})

router.beforeEach((to, from) => {
  if (to.meta.requiresAuth && !user.connected) {
    // this route requires auth, check if logged in
    // if not, redirect to login page.
    return {
      name: 'login',
      // save the location we were at to come back later
      query: { redirect: to.fullPath },
    }
  }
})

createApp(App)
  .use(router)
  .use(VueCookies)
  .component("VueFontawesome", FontAwesomeIcon)
  .use(Oruga, {
    ...bulmaConfig,
    iconPack: 'fas',
    iconComponent: 'vue-fontawesome'
  })
  .provide('user', { user, login, logout })
  .provide('apiURL', { apiURL })
  .mount("#app");

